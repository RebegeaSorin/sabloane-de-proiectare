﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Visitor
{
    class Program
    {
        static void Main(string[] args)
        {

            Customer c1 = new Customer() { Name = "Sorin" };
            Order order1 = new Order() { Name = "order1" };
            order1.Add(new Product() { Name = "vin", Price = 15 });
            order1.Add(new Product() { Name = "suc", Price = 10 });
            c1.Add(order1);

            Customer c2 = new Customer() { Name = "Stefan" };
            Order order2 = new Order() { Name = "order2" };
            order2.Add(new Product() { Name = "fasole", Price = 7 });
            order2.Add(new Product() { Name = "banane", Price = 8 });


            SupermarketReport visitor = new SupermarketReport();

            c1.Accept(visitor);
            c2.Accept(visitor);
            visitor.DisplayResults();
           

            Console.ReadKey(true);
            
        }
    }
}
