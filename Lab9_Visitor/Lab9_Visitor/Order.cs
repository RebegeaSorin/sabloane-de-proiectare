﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Visitor
{
    public class Order : IVisitable
    {
        public String Name { get; set; }

        public List<Product> products = new List<Product>();


        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
            
            foreach (Product prod in products)
            {
                prod.Accept(visitor);      
            }  

        }


        public void Add(Product prd)
        {
            products.Add(prd);
        }

    }
}
