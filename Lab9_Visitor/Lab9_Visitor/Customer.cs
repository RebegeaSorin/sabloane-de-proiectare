﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Visitor
{
    public class Customer : IVisitable
    {
        public string Name { get; set; }
        public List<Order> orders = new List<Order>();

        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);

            foreach (Order ord in orders)
            {
                ord.Accept(visitor);
            }
        }


        public void Add(Order order)
        {
            orders.Add(order);
        }
    }
}
