﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Visitor
{
    public class SupermarketReport : IVisitor
    {
        public int iProductNo = 0;
        public int iOrderNo = 0;
        public int iCustomerNo = 0;
        public double dTotalPrice = 0;

        public void DisplayResults()
        {
            Console.WriteLine("Number of customers: " + iCustomerNo);
            Console.WriteLine("Number of orders: " + iOrderNo );
            Console.WriteLine("Number of products: " + iProductNo);
        }

        public void Visit(Product product)
        {
            Console.WriteLine(product.Name);
            iProductNo++;
        }

        public void Visit(Order order)
        {
            Console.WriteLine(order.Name);
            iOrderNo++;
        }

        public void Visit(Customer customer)
        {
            Console.WriteLine(customer.Name);
            iCustomerNo++;
        }

    }
}
