﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7_State
{
    class Program
    {
        static void Main(string[] args)
        {
            VendingMachine vm = new VendingMachine();

            String value;

            Console.Write("Enter value ");
            value = Console.ReadLine();

            int intValue = Int32.Parse(value);

            switch (intValue)
            {
                case 1:
                    {
                        vm.UpdateState(EUserOption.InsertCoin);
                        break;
                    }
                case 2:
                    {
                        vm.UpdateState(EUserOption.EjectCoin);
                        break;
                    }
                case 3:
                    {
                        vm.UpdateState(EUserOption.BuyProduct);
                        break;
                    }
                case 4:
                    {
                        vm.UpdateState(EUserOption.FillMachine);
                        break;
                    }
                case 5:
                    {
                        vm.UpdateState(EUserOption.InspectMachine);
                        break;
                    }
                default:
                    {
                        vm.UpdateState(EUserOption.Exit);
                        break;
                    }
            }

        }
    }
}
