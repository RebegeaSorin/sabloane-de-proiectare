﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7_State
{
    public class SoldState : State
    {
        public SoldState(VendingMachine vm)
            : base(vm)
        {

        }

        public override bool InsertCoin()
        {
            Console.WriteLine("Coin entered");
            HasCoinState hasCoin = new HasCoinState(Machine);
            Machine.SetMachineState(hasCoin);

            return true;
        }


        public override bool EjectCoin()
        {
            Console.WriteLine("Coin cannot be ejected");
            return false;
        }


        public override bool BuyProduct()
        {
            Console.WriteLine("Cannot buy a product");
            return false;
        }


        public override bool Dispense()
        {

            Console.WriteLine("Dispense succes");

            NoCoinState noCoin = new NoCoinState(Machine);
            Machine.SetMachineState(noCoin);

            return true;
        }
    }
}
