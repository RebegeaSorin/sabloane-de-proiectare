﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7_State
{
    class NoCoinState : State
    {
        public NoCoinState(VendingMachine vm)
            : base(vm)
        {
        }

        public NoCoinState()
        {
        }

        public override bool InsertCoin()
        {
            Console.WriteLine("Coin inserted");

            HasCoinState hasCoin = new HasCoinState(Machine);
            Machine.SetMachineState(hasCoin);

            return true;
        }

        public override bool EjectCoin()
        {
            Console.WriteLine("Coin could not eject");
            return false;
        }

        public override bool BuyProduct()
        {
            Console.WriteLine("Cannot buy a product");
            return false;
        }


        public override bool Dispense()
        {
            Console.WriteLine("Cannot dispense");
            return false;
        }
    }
}
