﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7_State
{
    public class VendingMachine
    {
        private State machineState;
        private int capacity;

        public VendingMachine()
        {
            capacity = 0;
            machineState = new NoCoinState(this);
        }

        public void UpdateState(EUserOption option)
        {
            switch (option)
            {
                case EUserOption.InsertCoin:
                    InsertCoin();
                    break;
                case EUserOption.EjectCoin:
                    EjectCoin();
                    break;
                case EUserOption.BuyProduct:
                    BuyProduct();
                    break;
                case EUserOption.FillMachine:
                    ReFill();
                    break;
                case EUserOption.InspectMachine:
                    Inspect();
                    break;
                case EUserOption.Exit:
                    break;
                default:
                    break;
            }
        }

        public void ReFill()
        {
            Console.WriteLine("Please enter the value: ");
            String readValue = Console.ReadLine();

            capacity += Int32.Parse(readValue);
            machineState = new SoldState(this);
        }

        public bool InsertCoin()
        {
            Console.WriteLine("Insert coin");
            return machineState.InsertCoin();
        }

        public bool EjectCoin()
        {
            Console.WriteLine("Eject coin operation");
            return machineState.EjectCoin();
        }

        public bool BuyProduct()
        {
            Console.WriteLine("Buy product");

            if (machineState.BuyProduct())
            {
                capacity--;
                if (IsEmpty())
                    SetMachineState(new SoldOutState(this));
                return true;
            }
            return false;
        }

        public void Inspect()
        {
            Console.WriteLine("Capacity = " + capacity);
        }

        public bool IsEmpty()
        {
            if (capacity == 0 || capacity < 0)
            {
                return true;
            }

            return false;
        }

        public void SetMachineState(State state)
        {
            machineState = state;
        }

    }
}
