﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7_State
{
    public class SoldOutState : State
    {
        public SoldOutState(VendingMachine vm)
            : base(vm)
        {

        }   

        public override bool InsertCoin()
        {
            Console.WriteLine("Cannot insert coin");
            return false;
        }


        public override bool EjectCoin()
        {
            Console.WriteLine("Cannot eject coin");
            return false;
        }


        public override bool BuyProduct()
        {
            Console.WriteLine("Cannot buy a product");
            return false;
        }


        public override bool Dispense()
        {
            Console.WriteLine("Cannot dispense");
            return false;
        }
    }
}
