﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Proxy
{
    public interface IAccount
    {
        bool Deposit(int deposit);
        
        bool Withdrow(int Withdrow);

        bool DisplayBalance();


    }
}
