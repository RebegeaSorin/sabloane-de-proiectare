﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Proxy
{
    class SafeAccountProxy : IAccount
    {

        AccountProtected _account;
        string password = "1234";

        public bool Deposit(int deposit)
        {
            if (Autenticate())
            {
                _account.Deposit(deposit);
                return true;
            }
            return false;
        }

        public bool Withdrow(int Withdrow)
        {
            if (Autenticate())
            {
                _account.Withdrow(Withdrow);
                return true;
            }
            return false;
        }

        public bool DisplayBalance()
        {
            if (Autenticate())
            {
                _account.DisplayBalance();
                return true;
            }
            return false;
        }

        public bool Autenticate()
        {
            if (_account != null)
            {
                return true;
            }
            Console.WriteLine("Write Password !");
            string line = Console.ReadLine();
            while (line != password)
            {
                Console.WriteLine("Ai gresit parola te rog pune alta! Daca vrei sa iesi apasa N");
                line = Console.ReadLine();
                if (line == "N")
                {
                    //iesi din program!
                    return false;
                }
            }
            _account = new AccountProtected();
            return true;
        }

    }
}
