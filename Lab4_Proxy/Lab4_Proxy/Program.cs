﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Proxy
{
    class Program
    {
        static void Main(string[] args)
        {

            IAccount account = new SafeAccountProxy();

            account.Deposit(100);
            account.Withdrow(50);
            account.Withdrow(200);
            account.DisplayBalance();

            Console.ReadKey(true);

        }
    }
}
