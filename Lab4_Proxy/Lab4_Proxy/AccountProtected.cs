﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Proxy
{
    public class AccountProtected : IAccount
    {

        protected int Balance { get; set; }

        public bool Deposit(int deposit)
        {
            if (deposit > 0)
            {
                Console.WriteLine("Deposited succesful");
                Balance += deposit;
                return true;
            }

            Console.WriteLine("Deposit failed");
            return false;
        }

        public bool Withdrow(int Withdrow)
        {
            if (Balance < Withdrow)
            {
                Console.WriteLine("Withdrow failed");
                return false;
            }

            Console.WriteLine("Withdrow succesfull");
            Balance -= Withdrow;
            return true;

        }

        public bool DisplayBalance()
        {
            Console.WriteLine("Balance:  {0}$", Balance);
            return true;   
        }



    }
}
