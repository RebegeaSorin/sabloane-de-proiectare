﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._2_Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            InstantMessenger messenger = new InstantMessenger(new FacebookMessangerImpl());
            messenger.SendMessage("TEST MESSAGE");
            messenger.SendFile("FILE.TXT");

            messenger.MessagingImpl = new WhatsAppImpl();
            messenger.SendMessage("Another test");

            messenger.MessagingImpl = new SkypeImpl();
            messenger.SendFile("anotherfile.txt");

            Console.ReadKey(true);
        }
    }
}
