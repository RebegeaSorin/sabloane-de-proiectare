﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._2_Bridge
{
    public class WhatsAppImpl : IMessagingImpl
    {
        public void SendMessage(string message)
        {
            Console.WriteLine("Send message " + message + " using WhatsApp");
        }

        public void SendFile(string fileName)
        {
            Console.WriteLine("Send file " + fileName + " using WhatsApp");
        }

        public void VoiceCall()
        {
            Console.WriteLine("Voice call using WhatsApp");
        }

        public void VideoCall()
        {
            Console.WriteLine("Video call using WhatsApp");
        }
    }
}
