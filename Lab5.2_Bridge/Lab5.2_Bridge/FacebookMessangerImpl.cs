﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._2_Bridge
{
    public class FacebookMessangerImpl : IMessagingImpl
    {
        public void SendMessage(string message)
        {
            Console.WriteLine("Send message " + message + " using Facebook");
        }

        public void SendFile(string fileName)
        {
            Console.WriteLine("Send file " + fileName + " using Facebook");
        }

        public void VoiceCall()
        {
            Console.WriteLine("Voice call using Facebook");
        }

        public void VideoCall()
        {
            Console.WriteLine("Video call using Facebook");
        }
    }
}
