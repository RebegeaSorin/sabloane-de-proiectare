﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._2_Bridge
{
    public class SkypeImpl : IMessagingImpl
    {
        public void SendMessage(string message) 
        { 
            Console.WriteLine("Send message " + message + " using Skype"); 
        }

        public void SendFile(string fileName) 
        {
            Console.WriteLine("Send file " + fileName + " using Skype"); 
        }
        
        public void VoiceCall() 
        {
            Console.WriteLine("Voice call using Skype"); 
        } 
        
        public void VideoCall() 
        {
            Console.WriteLine("Video call using Skype"); 
        }
    }
}
