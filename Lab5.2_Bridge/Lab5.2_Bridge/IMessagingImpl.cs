﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._2_Bridge
{
    public interface IMessagingImpl
    {
        void SendMessage(string message); 
        void SendFile(string fileName); 
        void VoiceCall(); 
        void VideoCall();
    }
}
