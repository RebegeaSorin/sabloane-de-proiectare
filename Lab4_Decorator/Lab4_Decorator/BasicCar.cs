﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Decorator
{
    public class BasicCar : ICar
    {
        public ECarType Type
        {
            get;
            set;
        }

        public int Price;
        public string Color;
        public string Engine;

        public List<string> AccesTypeAccessories { get; set; }
        public List<string> AmbianceTypeAccessories { get; set; }
        public List<string> LaureateTypeAccessories { get; set; }


        public BasicCar()
        {
            Type = ECarType.Basic;

            AccesTypeAccessories = new List<string>();
            AmbianceTypeAccessories = new List<string>();
            LaureateTypeAccessories = new List<string>();
        }

        public void SetAccessory(EAccessoriesType type, string name, int price)
        {
            List<string> result = null;
            switch (type)
            {
                case EAccessoriesType.AccesType:
                    result = AccesTypeAccessories;
                    break;
                case EAccessoriesType.AmbianceType:
                    result = AmbianceTypeAccessories;
                    break;
                case EAccessoriesType.LaureateType:
                    result = LaureateTypeAccessories;
                    break;
                default:
                    break;
            }
            result.Add(name);
            Price += price;
        }

        public void Assemble()
        {
            Color = "White";
            Engine = "Diesel";
            Price = 7800;
        }

        public void Display()
        {
            Console.WriteLine("--------car informations--------");
            Console.WriteLine("Color: {0}", Color);
            Console.WriteLine("Engine: {0}", Engine);
            Console.WriteLine("Price: {0}", Price);
            Console.WriteLine("Type: {0}", Type);

            Console.WriteLine("-------- Acces ---------");
            foreach (var item in AccesTypeAccessories)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("-------- Ambiance ---------");
            foreach (var item in AmbianceTypeAccessories)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("-------- Laureate ---------");
            foreach (var item in LaureateTypeAccessories)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("\n");
        }
    }
}
