﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Decorator
{
    public enum ECarType
    {
        Basic = 0,
        Acces = 1,
        Ambiance = 2,
        Laureate = 3
    }

    public enum EAccessoriesType
    {
        AccesType = 0,
        AmbianceType = 1,
        LaureateType = 2
    }

    public interface ICar
    {
        ECarType Type { get; set; }

        void SetAccessory(EAccessoriesType type, string name, int price);
        void Assemble();
        void Display();
    }
}
