﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Decorator
{
    public class AmbianceDecorator : AccesDecorator
    {
        public AmbianceDecorator(ICar car) : base(car)
        {
            car.Type = ECarType.Ambiance;
        }

        public override void Assemble()
        {
            base.Assemble();

           // SetAccessory(EAccessoriesType.AccesType, "Sistem incalzire", 300);
           // SetAccessory(EAccessoriesType.AccesType, "Directie asistata",400);

            SetAccessory(EAccessoriesType.AmbianceType, "Abs", 1000);
        }



    }
}
