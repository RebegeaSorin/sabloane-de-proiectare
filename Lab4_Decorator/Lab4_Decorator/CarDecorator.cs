﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Decorator
{
    public abstract class CarDecorator : ICar
    {
        public ICar MyCar { get; set; }
        public ECarType Type { get; set; }

        public CarDecorator(ICar car)
        {
            MyCar = car;
        }

        public void SetAccessory(EAccessoriesType type, string name, int price)
        {
            MyCar.SetAccessory(type, name, price);
        }

        public virtual void Assemble()
        {
            MyCar.Assemble();
        }

        public void Display()
        {
            MyCar.Display();
        }
        
    }
}
