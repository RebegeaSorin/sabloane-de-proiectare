﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            CarDecorator accessCarDecorator = new AccesDecorator(new BasicCar());
            accessCarDecorator.Assemble();
            accessCarDecorator.Display();

            CarDecorator ambianceCarDecorator = new AmbianceDecorator(new BasicCar());
            ambianceCarDecorator.Assemble();
            ambianceCarDecorator.Display();

            CarDecorator laureateCarDecorator = new LaureanteDecorator(new BasicCar());
            laureateCarDecorator.Assemble();
            laureateCarDecorator.Display();

            Console.ReadKey(true);
        }
    }
}
