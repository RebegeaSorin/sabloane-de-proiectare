﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Decorator
{
    public class LaureanteDecorator : AmbianceDecorator
    {
        public LaureanteDecorator(ICar car) : base(car)
        {
            car.Type = ECarType.Laureate;
        }

        public override void Assemble()
        {
            base.Assemble();

            // SetAccessory(EAccessoriesType.AccesType, "Sistem incalzire", 300);
            // SetAccessory(EAccessoriesType.AccesType, "Directie asistata",400);

            //SetAccessory(EAccessoriesType.AmbianceType, "Abs", 300);

            SetAccessory(EAccessoriesType.LaureateType, "Tetiere fata reglabile", 1000);
        }
    }
}
