﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Factory.Factory
{
    public abstract class CarFactory
    {
        public static int _id = 0;

        public abstract Car GetCar(BrandType brandType, EngineType engine);
    }
}
