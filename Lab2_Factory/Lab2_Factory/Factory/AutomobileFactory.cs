﻿using Lab2_Factory.Factory;
using Lab2_Factory.Vehicles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Factory
{
    public class AutomobileFactory : CarFactory
    {
        public override Car GetCar(BrandType brandType, EngineType engine)
        {
            _id++;           

            return new Automobile(_id, brandType, engine);
        }

    }
}
