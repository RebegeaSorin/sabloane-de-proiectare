﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Factory
{
    public enum BrandType
    {
        Mercedes_Benz = 0,
        Volvo = 1,
        Renault = 2,
        Volkswagen =3
    }

    public enum CarType
    {
        Automobile = 0,
        Truck = 1
    }

    public enum EngineType
    {
        Diesel = 0,
        Gas = 1
    }
}
