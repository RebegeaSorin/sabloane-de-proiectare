﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Factory
{
    class Dealer
    {
        //public List<Car> TestDriveCars { get; set; }

        public List<Car> AvailableCars { get; set; }

        public Dealer(List<Car> carList)
        {
            this.AvailableCars = carList;
        }

        public void DoTestDrive(Car car)
        {
            Random randomKm = new Random();
            var km = randomKm.Next(1, 100);
            car.Role(km);
        }

        public void sellCar(Car car)
        {
            Console.WriteLine("Selling the car -> " + car.ToString());
            AvailableCars.Remove(car);
            Console.WriteLine("The car has been sold!");
        }

        public void Display(List<Car> cars)
        {

            cars.ForEach(car => Console.WriteLine(car.ToString()));
        }
    }
}
