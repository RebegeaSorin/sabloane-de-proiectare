﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Factory.Vehicles
{
    class Automobile : Car
    {

        public override CarType Type()
        {
            return CarType.Automobile;

        }

        public Automobile(int id, BrandType brandType, EngineType engine)
            : base(id, brandType, engine)
        {
 
        }
    }
}
