﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Factory.Vehicles
{
    class Truck : Car
    {

        public override CarType Type()
        {
            return CarType.Truck;

        }

        public Truck(int id, BrandType brandType, EngineType engine) : base(id, brandType, engine)
        {

        }
    }
}
