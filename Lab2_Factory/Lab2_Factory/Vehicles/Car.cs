﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Factory
{
    public abstract class Car
    {
        protected int _id;
        ///*abstract*/protected CarType _carType;
        public abstract CarType Type();
        protected BrandType _brandType;
        protected EngineType _engine;


        public int _Km { get; set; }

        public int ID { get { return this._id; } set { _id = value; } }

        public Car(int id, BrandType brandType, EngineType engine)
        {
            this._id = id;
            //this._carType = carType;
            this._brandType = brandType;
            this._engine = engine;
            this._Km = 0;
        }

        public void Role(int km)
        {
            _Km += km;
            Console.WriteLine(string.Format("\n{0} - has been rolled for {1}", this, km));
        }

        public override string ToString()
        {
            return string.Format("Id: {0}| Brand: {1}| Type: {2}| Engine: {3} | KM : {4}",
                                _id, _brandType.ToString(), this.Type(), _engine.ToString(), _Km);
        }


    }
}
