﻿using Lab2_Factory.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_Factory
{
    class Program
    {
        static void Main(string[] args)
        {
            
            CarFactory automobileFactory = new AutomobileFactory();
            CarFactory truckFactory = new TruckFactory();

            List<Car> carList = new List<Car>();
            Dealer dealer = new Dealer(carList);
            

            carList.Add(automobileFactory.GetCar(BrandType.Mercedes_Benz, EngineType.Diesel));
            carList.Add(automobileFactory.GetCar(BrandType.Volkswagen, EngineType.Gas));

            carList.Add(truckFactory.GetCar(BrandType.Renault, EngineType.Diesel));
            carList.Add(truckFactory.GetCar(BrandType.Volvo, EngineType.Diesel));


            dealer.Display(carList);

            dealer.DoTestDrive(carList[2]);
            Console.WriteLine();

            dealer.Display(carList);
       
            dealer.sellCar(carList[3]);

            dealer.Display(carList);

            Console.Read();

        }
    }
}
