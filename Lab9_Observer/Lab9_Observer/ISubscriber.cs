﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Observer
{
    public interface ISubscriber
    {
        String Name { get; set; }

        void Update(List<Product> products);
    }
}
