﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            OnlineStore onlineStore = new OnlineStore ();
            List<Product> products = new List<Product>();

            products.Add( new Product() { Name = "branza", Count = 3, Price = 3, Description = "de vaca" });
           
            Subscriber sorin = new Subscriber() { Name = "sorin"};
            Subscriber stefanita = new Subscriber() { Name = "stefanita" };

            onlineStore.Subcribe(sorin);
            onlineStore.Subcribe(stefanita);
           
            onlineStore.AddProduct(products);
            onlineStore.Inventory();

            products.Add(new Product() { Name = "ouo", Count = 20, Price = 1, Description = "de gaina" });
            products.Add(new Product() { Name = "lapte", Count = 5, Price = 4, Description = "pasteurizat" });

            products.Add(new Product() { Name = "calculator", Count = 1, Price = 400, Description = "-----" });
            

            onlineStore.AddProduct(products);
            onlineStore.Inventory();

            Console.ReadKey(true);



        }
    }
}
