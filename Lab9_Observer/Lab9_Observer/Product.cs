﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Observer
{
    public class Product
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }

        public override bool Equals(object obj)
        {
            Product p = obj as Product;
            if ((object)p == null)
            {
                return false;
            }
            return p.Name == Name && p.Description == Description && p.Price == Price;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Name:" + Name + " Price: " + Price + " Count:" + Count;
        }
    }
}
