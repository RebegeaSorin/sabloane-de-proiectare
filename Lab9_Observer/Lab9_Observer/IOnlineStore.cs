﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Observer
{
    public interface IOnlineStore
    {
        bool Subcribe(ISubscriber subscriber);
        bool UnSubscribe(ISubscriber subscriber);
        void Notify(List<Product> products);
        void AddProduct(List<Product> products);
        void Inventory();
    }
}
