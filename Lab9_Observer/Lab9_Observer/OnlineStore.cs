﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Observer
{
    public class OnlineStore : IOnlineStore
    {

        private List<ISubscriber> _subscribers = new List<ISubscriber>();
        public List<Product> _inventoryProducts = new List<Product>();


        public bool Subcribe(ISubscriber subscriber)
        {
            if (_subscribers.Contains(subscriber))
                return false;
            _subscribers.Add(subscriber);
            return true;
        }

        public bool UnSubscribe(ISubscriber subscriber)
        {
            if (!_subscribers.Contains(subscriber))
                return false;
            _subscribers.Remove(subscriber);
            return true;
        }

        public void Notify(List<Product> products)
        {
            foreach (ISubscriber subscriber in _subscribers)
                subscriber.Update(products);
        }

        public void Inventory()
        {
            double sum = 0;
            foreach (Product prod in _inventoryProducts)
                sum += (prod.Price * prod.Count);
            Console.WriteLine("Suma totala a produselor este = " + sum);
        }


        public void AddProduct(List<Product> products)
        {
            List<Product> newProducts = new List<Product>();


            foreach (Product prod in products)
            {
                int iIndexOf = _inventoryProducts.IndexOf(prod);
                if (iIndexOf == -1)
                {
                    _inventoryProducts.Add(prod);
                    newProducts.Add(prod);
                }
                else
                {
                    _inventoryProducts[iIndexOf].Count += prod.Count;
                }
            }


            if (newProducts.Count != 0)
                Notify(newProducts);
             
        }
    }
}
