﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9_Observer
{
    public class Subscriber : ISubscriber
    {
        public string Name { get; set; }

        public void Update(List<Product> products)
        {
            Console.WriteLine(Name);

            foreach (Product product in products)
                Console.WriteLine(product);
        }
    }
}
