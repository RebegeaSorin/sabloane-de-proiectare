﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1_singleton
{
    class WorkerThread
    {
        private static LoadBalancer _loadBalancer;

        static WorkerThread()
        {
            _loadBalancer = LoadBalancer.GetInstance();
        }

        public static void PostRequest(string workerThreadName)
        {
            for (int requestNumber = 1; requestNumber <= 100; requestNumber++)
            {
                var result = _loadBalancer.SolveRequest(GetRequestName(requestNumber, workerThreadName));
                Console.WriteLine(result);
            }
        }

        private static string GetRequestName(int requestNumber, string name)
        {
            return string.Format("Worker thread name: {0}, Request index: {1}", name, requestNumber);
        }
    }
}
