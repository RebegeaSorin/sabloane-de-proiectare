﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1_singleton
{
    class Server
    {
        static Random rnd = new Random();
        public string Name {get; set;}
        public string IpAdress{get; set;}
        public int NumOfRequest {get; set;}
        public int OverallTime { get; set; }
        public int MinProcessTime { get; set; }
        public int MaxProcessTime { get; set; }
        public double AvgTime { get; set; }

        public void Reqeust(string request)
        {
            NumOfRequest++;
            Random rnd = new Random();
            
            int requestTimeFofProcess = rnd.Next(500,1000);

            OverallTime += requestTimeFofProcess;
            System.Threading.Thread.Sleep(requestTimeFofProcess);


            if (requestTimeFofProcess < MinProcessTime)
                MinProcessTime = requestTimeFofProcess;

            if (requestTimeFofProcess > MaxProcessTime)
                MaxProcessTime = requestTimeFofProcess;

          
            Console.WriteLine("Solved : " + request);
        }

        public void ServerStatistic()
        {
            Console.WriteLine("\nServer name = " + Name + " with " + NumOfRequest +" number of requests");
            Console.WriteLine("The overall time processed time was = " + OverallTime);
            if (MinProcessTime == 9999)
                MinProcessTime = 0;
            Console.WriteLine("The lowest process time was = " + MinProcessTime);
            Console.WriteLine("The highest process time was = " + MaxProcessTime);
            
            if(OverallTime != 0)
                AvgTime = OverallTime / NumOfRequest;

            Console.WriteLine("The medium time for process a request was = " + AvgTime + "\n");

        }

    }
}
