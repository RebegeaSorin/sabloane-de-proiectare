﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1_singleton
{
    class LoadBalancer
    {
        private static LoadBalancer instance;
        public List<Server> myServers { get; set; }
        Random rnd = new Random();
        private bool MinFlag = true;

        private LoadBalancer() 
        {
            myServers = new List<Server>();
            for (int i = 0; i < 10; i++)
                myServers.Add(new Server() { Name = "Server"+i, MinProcessTime = 9999 });
        }

        public static LoadBalancer GetInstance()
        {
            if (instance == null)
                instance = new LoadBalancer();
            return instance;
        }

        public string SolveRequest(string nextRequest)
        {
            Random random = new Random();
            Server RequestServer = myServers[0];
            int minRequest = myServers[0].NumOfRequest;

            if (MinFlag)
            {
                foreach (Server server in myServers)
                {
                    if (server.NumOfRequest < minRequest)
                    {
                        minRequest = server.NumOfRequest;
                        RequestServer = server;
                    }
                }
            }
            else 
            {
                int serverNumber = rnd.Next(1, 10);
                RequestServer = myServers.ElementAt(serverNumber);
            }

            RequestServer.Reqeust(nextRequest);
            return "Redirecting to server -> " + RequestServer.Name + " with number of request = " +RequestServer.NumOfRequest;
        
        }

    }
}
