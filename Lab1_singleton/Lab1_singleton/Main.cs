﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab1_singleton
{
    class MainAsd
    {
        private static LoadBalancer _loadBalancer = LoadBalancer.GetInstance();
        
        public static void Main(string[] args)
        { 

            var allTasks = new List<Task>();

            for (int i = 0; i < 5; ++i)
            {
                var task = Task.Factory.StartNew(() =>
                {
                    WorkerThread.PostRequest(i.ToString());
                });

                allTasks.Add(task);
            }

            Task.WaitAll(allTasks.ToArray());

            var servers = LoadBalancer.GetInstance().myServers;
            foreach (var server in servers)
            {
                server.ServerStatistic();
            }

            Console.ReadKey(true);
            
        }

    }
}
