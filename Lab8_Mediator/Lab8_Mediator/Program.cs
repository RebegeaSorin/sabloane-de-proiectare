﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8_Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            IChatroom chatroom = new Chatroom();

            User user1 = new User() { Name = "Sorin" };
            User user2 = new User() { Name = "Stefan" };
            User user3 = new User() { Name = "Alina" };
            User user4 = new User() { Name = "Victoras" };

            User admin = new User() { Name = "admin" };

            chatroom.Register(user1);
            chatroom.Register(user2);
            chatroom.Register(user3);
            chatroom.Register(user4);
            chatroom.Register(admin);

            Console.WriteLine();

            user1.Send(user2, "Salut!");
            user2.Send(user1, "Salut si tie!");

            Console.WriteLine();

            chatroom.UnRegister(user1);
            
            Console.WriteLine();

            admin.Send(admin, "Mesaj de la admin!");

            Console.WriteLine();
            
            chatroom.UnRegister(user2);

            Console.ReadKey(true);

        }
    }
}
