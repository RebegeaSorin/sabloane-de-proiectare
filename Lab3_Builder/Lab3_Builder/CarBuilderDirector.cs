﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_Builder
{
    class CarBuilderDirector
    {
        private ICarBuilder _carBuilder { get; set; }

        public CarBuilderDirector(ICarBuilder carBuilder)
        {
            this._carBuilder = carBuilder;
        }

        public void Construct(Enums.CarModel carModel)
        {
            _carBuilder.SetColor("Alb");
            _carBuilder.SetModel(carModel);
            _carBuilder.SetEngine("Diesel");
        }

        public Car GetResult()
        {
            return _carBuilder.GetCar();
        }


    }
}
