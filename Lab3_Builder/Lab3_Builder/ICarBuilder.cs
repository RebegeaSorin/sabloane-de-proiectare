﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Lab3_Builder
{
    public interface ICarBuilder
    {

        Car GetCar();
        void SetColor(string color);
        void SetModel(Enums.CarModel carModel);
        void SetEngine(string engine);
    }
}
