﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_Builder
{
    public class Enums
    {

        public enum CarModel
        {
            Sedan = 0,
            Hatchback = 1,
            Pickup = 2,
            SUV = 3,
            Crossover = 4
        }


    }
}
