﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_Builder
{
    public class Car : ICarPrototype
    {
        public string _color;
        public Enums.CarModel _model;
        public string _engine;

        public override string ToString()
        {
            return string.Format("Model:{0}  \nColor:{1}  \nEngine:{2}\n",
                _model, _color, _engine);
        }


        private bool _isClone = false;

        public Car Clone()
        {
            var result = MemberwiseClone() as Car;
            result._color = "Albastru";
            result._isClone = true;
            return result;
        }

        public bool IsClone
        {
            get
            {
                return _isClone;
            }
            set
            {
                _isClone = value;
            }
        }

    }
}
