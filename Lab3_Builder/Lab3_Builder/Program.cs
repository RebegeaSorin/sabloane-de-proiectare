﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_Builder
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Car> cars = new List<Car>();
            ICarBuilder builder = new CarBuilder();
            CarBuilderDirector director = new CarBuilderDirector(builder);

            for (int i = 0; i < 5; i++)
            {
                var model = (Enums.CarModel)i;
                director.Construct(model);
                var car = director.GetResult();
                cars.Add(car);
                Console.WriteLine(car.ToString());

            }

            var cloneCar = cars.Last().Clone();

            if (cloneCar.IsClone)
            {

                Console.WriteLine("The next car is a clone");
                Console.WriteLine(cloneCar.ToString());
            }

            Console.ReadKey(true);
        }
    }
}
