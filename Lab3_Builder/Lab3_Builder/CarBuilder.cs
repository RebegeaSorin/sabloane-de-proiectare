﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3_Builder
{
    public class CarBuilder : ICarBuilder
    {
        Car _car = new Car();
        public Car GetCar()
        {
            return this._car;
        }

        public void SetColor(string color)
        {
            _car._color = color;
        }

        public void SetModel(Enums.CarModel carModel)
        {
            _car._model = carModel;
        }

        public void SetEngine(string engine)
        {
            _car._engine = engine;
        }
    }
}
