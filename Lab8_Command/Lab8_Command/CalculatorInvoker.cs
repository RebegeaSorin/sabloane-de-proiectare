﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8_Command
{
    public class CalculatorInvoker
    {
        private ICommand _command;

        public CalculatorInvoker()
        {
            _command = new CalculatorCommand();
        }

        public void Compute(char operation, double value)
        {
            _command.Operand = value;
            _command.ArithmOperator = operation;
            _command.Execute();
        }
    }
}
