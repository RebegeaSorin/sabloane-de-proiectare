﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8_Command
{
    public class Calculator
    {
        private double _currentValue = 0;

        public void DoOperation(char operation, double value)
        {
            Console.Write("{0}{1}{2}", _currentValue, operation, value);

            switch (operation)
            {
                case '+': 
                { 
                    _currentValue += value; 
                    break; 
                }
                case '-': 
                { 
                    _currentValue -= value;
                    break; 
                }
                case '*': 
                { 
                    _currentValue *= value; 
                    break;
                }
                case '/': 
                { 
                    _currentValue /= value;
                    break;
                }
            }

            Console.WriteLine("=" + _currentValue);
        }
    }
}
