﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8_Command
{
    class Program
    {
        static void Main(string[] args)
        {
      
            CalculatorInvoker invoker = new CalculatorInvoker();

            invoker.Compute('+', 30);
            invoker.Compute('*', 2);
            invoker.Compute('/', 5);

            Console.ReadKey(true);

        }
    }
}
