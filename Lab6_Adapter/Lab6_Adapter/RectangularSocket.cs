﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Adapter
{
    public class RectangularSocket
    {
        public string GetSupply(string firstWire, string secondWire)
        {
            return string.Format("Rectangular Socket {0} - {1}", firstWire, secondWire);
        }
    }
}
