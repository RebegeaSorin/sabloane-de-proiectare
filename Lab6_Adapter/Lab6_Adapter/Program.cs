﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            CylindricalPlug plug = new CylindricalPlug("faza", "null");

            Console.WriteLine(plug.GetPowerSupply(EAdapterType.ClassAdapter));
            Console.WriteLine(plug.GetPowerSupply(EAdapterType.ObjectAdapter));

            Console.ReadKey(true);
        }
    }
}
