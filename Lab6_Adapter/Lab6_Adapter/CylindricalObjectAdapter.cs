﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Adapter
{
    public class CylindricalObjectAdapter
    {
        private RectangularSocket _socket=new RectangularSocket();

        public string GetPowerSupply(string firstWire,string secondWire)
        {
            return string.Format("{0} - {1} ", EAdapterType.ObjectAdapter, _socket.GetSupply(firstWire, secondWire));
        }
    }    
}

