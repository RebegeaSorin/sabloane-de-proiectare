﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Adapter
{
    class CylindricalPlug
    {
        private string _firstWire;
        private string _secondWire;

        public CylindricalPlug(string firstWire, string secondWire)
        {
            _firstWire = firstWire;
            _secondWire = secondWire;
        }

        public string GetPowerSupply(EAdapterType eAdapter)
        {
            switch (eAdapter)
            {
                case EAdapterType.ClassAdapter:
                {
                    CylindricalClassAdapter adapter = new CylindricalClassAdapter();
                    return adapter.GetPowerSupply(_firstWire, _secondWire);
                }
                case EAdapterType.ObjectAdapter:
                {
                    CylindricalObjectAdapter adapter = new CylindricalObjectAdapter();
                    return adapter.GetPowerSupply(_firstWire, _secondWire);
                }
            }
            return null;
        }
    }
}
