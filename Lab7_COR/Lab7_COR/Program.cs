﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7_COR
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee developer = new Developer();
            Employee teamLeader = new TeamLeader();
            Employee projectLeader = new ProjectLeader();
            Employee departamentDirector = new DepartamentDirector();

            VacationRequest vr = new VacationRequest(DateTime.Now, new DateTime(2017, 4, 19));

            developer.Supervisor = teamLeader;
            teamLeader.Supervisor = projectLeader;
            projectLeader = departamentDirector;

            Console.WriteLine(vr.GetNumberOfDaysOff());
            developer.ApplyVacation(vr);
            
            Console.ReadKey(true);
        }
    }
}
