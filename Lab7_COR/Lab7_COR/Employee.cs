﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7_COR
{
    public abstract class Employee
    {
        public Employee Supervisor { get; set; }

        public void ApplyVacation(VacationRequest vacationRequest)
        {
            ApproveVacation(vacationRequest);
        }

        public bool ApproveVacation(VacationRequest vacationRequest)
        {
            if (vacationRequest.GetNumberOfDaysOff() <= GetMaxVacationCanAprove())
            {
                Console.WriteLine("A fost aprobata cererea" + this.GetType());
                return true;
            }
            else
            {
                if (Supervisor != null)
                {
                    return Supervisor.ApproveVacation(vacationRequest);
                }
                else
                {
                    Console.WriteLine("Cererea nu a fost aprobata");
                }
            }

            return false;
        }

        public abstract int GetMaxVacationCanAprove();

    }
}
