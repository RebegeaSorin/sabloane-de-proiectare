﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7_COR
{
    public class VacationRequest
    {
        private static int _iLastRequestNumber=0;

        public DateTime StartDay { get; set; }
        public DateTime EndDay { get; set; }
        public int RequestNumber;

        public VacationRequest(DateTime d,DateTime d2)
        {
            StartDay = d;
            EndDay = d2;
            RequestNumber = ++_iLastRequestNumber;
        }

        public int GetNumberOfDaysOff()
        {
            return (EndDay - StartDay).Days;
        }
    }
}
