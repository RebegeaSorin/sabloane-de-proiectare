﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._1_Composite
{
    public class Leaf : Vegetation 
    {
        public Leaf(string name, string parentName)
            : base(name, parentName)
        {
 
        }

        public override void Add(Vegetation v)
        {
 
        }

        public override void Remove(Vegetation v)
        {
           
        }

        public override void Display(int k)
        {
            Console.Write(new String('*', k));
            Console.WriteLine("{0} from {1}", name, parentName);    
        
        }
    }
}
