﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._1_Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            Vegetation tree = new Tree("root", "");
            Vegetation leaf = new Leaf("leaf", "branch");
            Vegetation branch = new Branch("branch", "root");
            Vegetation leaf2 = new Leaf("leaf2", "branch");
            Vegetation branch2 = new Branch("branch2", "root");
            Vegetation leaf3 = new Leaf("leaf3", "branch2");
            Vegetation leaf4 = new Leaf("leaf4", "branch2");
            Vegetation leaf5 = new Leaf("leaf5", "branch2");
            Vegetation branch3 = new Branch("branch3", "branch2");

            branch.Add(leaf);
            branch.Add(leaf2);
            branch2.Add(leaf3);
            branch2.Add(leaf4);
            branch2.Add(leaf4);
            tree.Add(branch);
            tree.Add(branch2);
            branch2.Add(branch3);
            tree.Display(1);

            Console.ReadKey(true);
        }
    }
}
