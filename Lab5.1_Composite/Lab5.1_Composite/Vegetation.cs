﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._1_Composite
{
    public abstract class Vegetation
    {
        protected string name;
        protected string parentName;

        public Vegetation(string name, string parentName)
        {
            this.name = name;
            this.parentName = parentName;
        }

        public abstract void Add(Vegetation v);
        public abstract void Remove(Vegetation v);
        public abstract void Display(int k);

    }
}
