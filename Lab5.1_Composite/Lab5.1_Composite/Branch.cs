﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._1_Composite
{
    public class Branch : Vegetation
    {
        private List<Vegetation> children = new List<Vegetation>();

        public Branch(string name, string parentName)
            : base(name, parentName)
        {
 
        }

        public override void Add(Vegetation v)
        {
            children.Add(v);
        }

        public override void Remove(Vegetation v)
        {
            children.Remove(v);
        }

        public override void Display(int k)
        {
            Console.Write(new String('*', k));
            Console.Write("{0} from {1}\n", name, parentName);

            k++;
            foreach (var child in children)
            {
                child.Display(k);

            }
        }
    }
}
