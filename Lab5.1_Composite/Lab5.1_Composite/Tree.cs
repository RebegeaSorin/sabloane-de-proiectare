﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5._1_Composite
{
    public class Tree : Branch
    {
        public Tree(string name,string parentName):base(name,parentName)
        {

        }

        public override void Display(int k)
        {
            Console.WriteLine("my tree");
            base.Display(k);           
         
        }
    }
}
