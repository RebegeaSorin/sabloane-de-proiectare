﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Flyweight
{
    public abstract class CashRegister
    {
        private Dictionary<double, Money> _sharedMoney = new Dictionary<double, Money>();
        private Money _unsharedMoney;
            
        public abstract Money CreateNewMoney();
        public abstract bool IsSharedValue(double sum);
        
        public CashRegister()
        {
            _unsharedMoney = CreateNewMoney();
        }

        protected Money Lookup(double value)
        {
            if (_sharedMoney.ContainsKey(value) == false)
            {
                _sharedMoney[value] = CreateNewMoney();
            }

            return _sharedMoney[value];
        }

        public void CashIn(double value)
        {
            if (IsSharedValue(value))
            {
                Lookup(value).TotalCacheValue += value;
            }
            else
            {
                _unsharedMoney.TotalCacheValue += value;
            }
        }

        public void CashOut(double value)
        {
            if (IsSharedValue(value))
            {
                Lookup(value).TotalCacheValue -= value;
            }
            else
            {
                _unsharedMoney.TotalCacheValue -= value;
            }

        }

        public double GetTotalCache()
        {
            double sum = 0;

            foreach (var money in _sharedMoney)
            {
                sum += money.Value.TotalCacheValue;
            }

            sum += _unsharedMoney.TotalCacheValue;

            return sum;
        }

    }
}
