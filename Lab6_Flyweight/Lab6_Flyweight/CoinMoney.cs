﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Flyweight
{
    public class CoinMoney : Money
    {
        public override EMoneyType GetMoneyType()
        {
            return EMoneyType.Coin;
        }

        public static bool IsSharedValue(double sum)
        {
            List<double> money = new List<double>() { 0.01, 0.05, 0.1, 0.5 };

            return money.Contains(sum);
        }
    }
}
