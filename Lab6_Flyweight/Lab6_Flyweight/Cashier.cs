﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Flyweight
{
    public class Cashier
    {
        private CashRegister _coinRegister = new CacheRegisterCoin();
        private CashRegister _paperRegister = new CacheRegisterPaper();
        private CashRegister _cardRegister = new CacheRegisterCard();

        public void CashIn(double value, EMoneyType type)
        {
            switch (type)
            {
                case EMoneyType.Coin:
                {

                    _coinRegister.CashIn(value);
                    break;
                }
                case EMoneyType.Paper:
                {

                    _paperRegister.CashIn(value);
                    break;
                }
                case EMoneyType.Card:
                {

                    _cardRegister.CashIn(value);
                    break;
                }
            }
        }

        public void CashOut(double sum, EMoneyType type)
        {
            if (GetTotalCache() - sum < 0)
            {
                Console.WriteLine("Nu avem de unde sa scoatem CASA ESTE NULA");
                return;
            }

            switch (type)
            {
                case EMoneyType.Coin:
                {

                    _coinRegister.CashOut(sum);
                    break;
                }
                case EMoneyType.Paper:
                {

                    _paperRegister.CashOut(sum);
                    break;
                }
                case EMoneyType.Card:
                {

                    _cardRegister.CashOut(sum);
                    break;
                }
            }
        }

        public double GetTotalCache()
        {
            return _cardRegister.GetTotalCache() + _coinRegister.GetTotalCache() + _paperRegister.GetTotalCache();

        }

    }
}
