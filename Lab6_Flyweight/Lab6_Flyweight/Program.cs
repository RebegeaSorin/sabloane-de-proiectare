﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Flyweight
{
    class Program
    {
        static void Main(string[] args)
        {

            Cashier cashier = new Cashier();
            cashier.CashIn(500, EMoneyType.Paper);
            cashier.CashIn(10, EMoneyType.Card);
            cashier.CashIn(0.10, EMoneyType.Coin);
            cashier.CashIn(0.6, EMoneyType.Coin);

            cashier.CashOut(500, EMoneyType.Card);
            cashier.CashOut(0.10, EMoneyType.Coin);

            cashier.CashOut(20, EMoneyType.Card);

            Console.WriteLine(cashier.GetTotalCache());

            Console.ReadKey(true);

        }
    }
}
