﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Flyweight
{
    public class CacheRegisterCard : CashRegister 
    {

       public override bool IsSharedValue(double value)
       {
           return false;   
        }

        public override Money CreateNewMoney()
        {
            return new PaperMoney();
        }
    }
}
